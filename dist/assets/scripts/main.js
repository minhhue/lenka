/**
 * @license
 * Copyright Wesam Gerges. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */

class slideToUnlock {
    constructor(el, options) {
        this.$el = el;
        this.$drag;
        this.start = false;
        this.leftEdge;
        this.rightEdge;
        this.mouseX;

        this.settings = {
            // unlockText    : "Slide To Unlock",
            useData: false,
            unlockfn: function () {
                console.log('unlock');
            },
            lockfn: function () {},
            allowToLock: true,
            status: false

            // Establish our default settings
        };this.settings = Object.assign(this.settings, options);
        if (this.settings.useData) {
            if (!('unlockText' in this.settings) && this.$el.data('unlock-text')) {
                this.settings.unlockText = this.$el.data('unlock-text');
            }
            if (!('lockText' in this.settings) && this.$el.data('lock-text')) {
                this.settings.lockText = this.$el.data('lock-text');
            }

            this.settings.status = this.$el.data('status');
        }

        if (!('lockText' in this.settings)) {
            this.settings.lockText = 'Slide To Unlock';
            // this.settings.lockText = ' ';
        }

        if (!('unlockText' in this.settings)) {
            this.settings.unlockText = this.settings.lockText;
        }

        this.init();
        return this;
    }

    init() {
        console.log('useData init', this.settings.useData);
        this.$el.addClass('slideToUnlock');
        this.leftEdge = this.$el.offset().left;
        this.rightEdge = this.leftEdge + this.$el.outerWidth();

        this.$el.addClass('locked');
        this.$el.html('');{}

        this.$el.append('<div class=\'progressBar unlocked\'></div>');
        this.$el.append('<div class=\'text\'>' + this.settings.lockText + '</div>');
        this.$el.append('<div class=\'drag locked_handle \'>  </div>');

        this.$text = this.$el.find('.text');
        this.$drag = this.$el.find('.drag');
        this.$progressBar = this.$el.find('.progressBar');

        this.$drag.on('mousedown.slideToUnlock touchstart.slideToUnlock', this.touchStart.bind(this));

        if (this.settings.status) {
            this.$drag.css({ left: 'auto', right: 0 });
            this.$progressBar.css({ width: '100%' });
        }
    }

    touchStart(event = window.event) {
        this.start = true;
        this.leftEdge = Math.trunc(this.$el.offset().left);
        this.rightEdge = Math.trunc(this.leftEdge + this.$el.outerWidth());

        $(document).on('mousemove.slideToUnlock touchmove.slideToUnlock', this.touchMove.bind(this));
        $(document).on('mouseup.slideToUnlock touchend.slideToUnlock', this.touchEnd.bind(this));
        this.mouseX = event.type == 'mousedown' ? event.pageX : event.originalEvent.touches[0].pageX;

        event.preventDefault();
    }

    touchMove(event = window.event) {
        if (!this.start) return;
        var X = event.type == 'mousemove' ? event.pageX : event.originalEvent.touches[0].pageX;
        var changeX = X - this.mouseX;
        var edge = Math.trunc(this.$drag.offset().left) + changeX;
        this.mouseX = X;

        if (edge < this.leftEdge) {
            if (this.settings.status) this.settings.lockfn(this.$el);
            this.$text.text(this.settings.lockText);
            this.$drag.removeClass('unlocked_handle').addClass('locked_handle');
            this.start = false;
            this.settings.status = false;
            this.touchEnd();
            return;
        }

        if (edge > this.rightEdge - this.$drag.outerWidth()) {
            if (!this.settings.status) {
                this.settings.unlockfn(this.$el);
            }
            this.$text.text(this.settings.unlockText);
            this.$drag.removeClass('locked_handle').addClass('unlocked_handle');
            if (!this.settings.allowToLock) {
                this.$drag.off('mousedown.slideToUnlock touchstart.slideToUnlock');
            }
            this.settings.status = true;
            this.start = false;
            this.touchEnd();
            return;
        }

        this.$drag.offset({ left: edge });
        this.$progressBar.css({ 'width': edge - this.$el.offset().left + this.$drag.outerWidth() });

        event.stopImmediatePropagation();
    }

    touchEnd(event = window.event) {
        this.start = false;
        this.mouseX = 0;
        if (!this.settings.status) {
            this.$drag.animate({ left: 0, 'margin-left': 0 });
            if (this.$progressBar.width() > this.$drag.width()) {
                this.$progressBar.animate({ width: this.$drag.width() }, function () {
                    this.$progressBar.css({ width: 0 });
                }.bind(this));
            }
        }

        if (this.settings.status) {
            this.$drag.animate({ 'left': '100%', 'margin-left': '-50px' });
            this.$progressBar.animate({ width: '100%' });
        }

        $(document).off('mousemove.slideToUnlock touchmove.slideToUnlock');
        $(document).off('mouseup.slideToUnlock touchend.slideToUnlock');
        event.stopImmediatePropagation();
    }
};
/*
* Add it to Jquery
*/
(function ($) {

    $.fn.slideToUnlock = function (options) {
        $.each(this, function (i, el) {
            new slideToUnlock($(el), options);
        });
        return this;
    };
})(jQuery);
//# sourceMappingURL=jquery.slideToUnlock.js.map

/*!
 * jQuery Mousewheel 3.1.12
 *
 * Copyright 2014 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 */

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS style for Browserify
        module.exports = factory;
    } else {
        // Browser globals
        factory(jQuery);
    }
})(function ($) {

    var toFix = ['wheel', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll'],
        toBind = 'onwheel' in document || document.documentMode >= 9 ? ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'],
        slice = Array.prototype.slice,
        nullLowestDeltaTimeout,
        lowestDelta;

    if ($.event.fixHooks) {
        for (var i = toFix.length; i;) {
            $.event.fixHooks[toFix[--i]] = $.event.mouseHooks;
        }
    }

    var special = $.event.special.mousewheel = {
        version: '3.1.12',

        setup: function () {
            if (this.addEventListener) {
                for (var i = toBind.length; i;) {
                    this.addEventListener(toBind[--i], handler, false);
                }
            } else {
                this.onmousewheel = handler;
            }
            // Store the line height and page height for this particular element
            $.data(this, 'mousewheel-line-height', special.getLineHeight(this));
            $.data(this, 'mousewheel-page-height', special.getPageHeight(this));
        },

        teardown: function () {
            if (this.removeEventListener) {
                for (var i = toBind.length; i;) {
                    this.removeEventListener(toBind[--i], handler, false);
                }
            } else {
                this.onmousewheel = null;
            }
            // Clean up the data we added to the element
            $.removeData(this, 'mousewheel-line-height');
            $.removeData(this, 'mousewheel-page-height');
        },

        getLineHeight: function (elem) {
            var $elem = $(elem),
                $parent = $elem['offsetParent' in $.fn ? 'offsetParent' : 'parent']();
            if (!$parent.length) {
                $parent = $('body');
            }
            return parseInt($parent.css('fontSize'), 10) || parseInt($elem.css('fontSize'), 10) || 16;
        },

        getPageHeight: function (elem) {
            return $(elem).height();
        },

        settings: {
            adjustOldDeltas: true, // see shouldAdjustOldDeltas() below
            normalizeOffset: true // calls getBoundingClientRect for each event
        }
    };

    $.fn.extend({
        mousewheel: function (fn) {
            return fn ? this.bind('mousewheel', fn) : this.trigger('mousewheel');
        },

        unmousewheel: function (fn) {
            return this.unbind('mousewheel', fn);
        }
    });

    function handler(event) {
        var orgEvent = event || window.event,
            args = slice.call(arguments, 1),
            delta = 0,
            deltaX = 0,
            deltaY = 0,
            absDelta = 0,
            offsetX = 0,
            offsetY = 0;
        event = $.event.fix(orgEvent);
        event.type = 'mousewheel';

        // Old school scrollwheel delta
        if ('detail' in orgEvent) {
            deltaY = orgEvent.detail * -1;
        }
        if ('wheelDelta' in orgEvent) {
            deltaY = orgEvent.wheelDelta;
        }
        if ('wheelDeltaY' in orgEvent) {
            deltaY = orgEvent.wheelDeltaY;
        }
        if ('wheelDeltaX' in orgEvent) {
            deltaX = orgEvent.wheelDeltaX * -1;
        }

        // Firefox < 17 horizontal scrolling related to DOMMouseScroll event
        if ('axis' in orgEvent && orgEvent.axis === orgEvent.HORIZONTAL_AXIS) {
            deltaX = deltaY * -1;
            deltaY = 0;
        }

        // Set delta to be deltaY or deltaX if deltaY is 0 for backwards compatabilitiy
        delta = deltaY === 0 ? deltaX : deltaY;

        // New school wheel delta (wheel event)
        if ('deltaY' in orgEvent) {
            deltaY = orgEvent.deltaY * -1;
            delta = deltaY;
        }
        if ('deltaX' in orgEvent) {
            deltaX = orgEvent.deltaX;
            if (deltaY === 0) {
                delta = deltaX * -1;
            }
        }

        // No change actually happened, no reason to go any further
        if (deltaY === 0 && deltaX === 0) {
            return;
        }

        // Need to convert lines and pages to pixels if we aren't already in pixels
        // There are three delta modes:
        //   * deltaMode 0 is by pixels, nothing to do
        //   * deltaMode 1 is by lines
        //   * deltaMode 2 is by pages
        if (orgEvent.deltaMode === 1) {
            var lineHeight = $.data(this, 'mousewheel-line-height');
            delta *= lineHeight;
            deltaY *= lineHeight;
            deltaX *= lineHeight;
        } else if (orgEvent.deltaMode === 2) {
            var pageHeight = $.data(this, 'mousewheel-page-height');
            delta *= pageHeight;
            deltaY *= pageHeight;
            deltaX *= pageHeight;
        }

        // Store lowest absolute delta to normalize the delta values
        absDelta = Math.max(Math.abs(deltaY), Math.abs(deltaX));

        if (!lowestDelta || absDelta < lowestDelta) {
            lowestDelta = absDelta;

            // Adjust older deltas if necessary
            if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
                lowestDelta /= 40;
            }
        }

        // Adjust older deltas if necessary
        if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
            // Divide all the things by 40!
            delta /= 40;
            deltaX /= 40;
            deltaY /= 40;
        }

        // Get a whole, normalized value for the deltas
        delta = Math[delta >= 1 ? 'floor' : 'ceil'](delta / lowestDelta);
        deltaX = Math[deltaX >= 1 ? 'floor' : 'ceil'](deltaX / lowestDelta);
        deltaY = Math[deltaY >= 1 ? 'floor' : 'ceil'](deltaY / lowestDelta);

        // Normalise offsetX and offsetY properties
        if (special.settings.normalizeOffset && this.getBoundingClientRect) {
            var boundingRect = this.getBoundingClientRect();
            offsetX = event.clientX - boundingRect.left;
            offsetY = event.clientY - boundingRect.top;
        }

        // Add information to the event object
        event.deltaX = deltaX;
        event.deltaY = deltaY;
        event.deltaFactor = lowestDelta;
        event.offsetX = offsetX;
        event.offsetY = offsetY;
        // Go ahead and set deltaMode to 0 since we converted to pixels
        // Although this is a little odd since we overwrite the deltaX/Y
        // properties with normalized deltas.
        event.deltaMode = 0;

        // Add event and delta to the front of the arguments
        args.unshift(event, delta, deltaX, deltaY);

        // Clearout lowestDelta after sometime to better
        // handle multiple device types that give different
        // a different lowestDelta
        // Ex: trackpad = 3 and mouse wheel = 120
        if (nullLowestDeltaTimeout) {
            clearTimeout(nullLowestDeltaTimeout);
        }
        nullLowestDeltaTimeout = setTimeout(nullLowestDelta, 200);

        return ($.event.dispatch || $.event.handle).apply(this, args);
    }

    function nullLowestDelta() {
        lowestDelta = null;
    }

    function shouldAdjustOldDeltas(orgEvent, absDelta) {
        // If this is an older event and the delta is divisable by 120,
        // then we are assuming that the browser is treating this as an
        // older mouse wheel event and that we should divide the deltas
        // by 40 to try and get a more usable deltaFactor.
        // Side note, this actually impacts the reported scroll distance
        // in older browsers and can cause scrolling to be slower than native.
        // Turn this off by setting $.event.special.mousewheel.settings.adjustOldDeltas to false.
        return special.settings.adjustOldDeltas && orgEvent.type === 'mousewheel' && absDelta % 120 === 0;
    }
});
//# sourceMappingURL=jquery.mousewheel.js.map

!function (e) {
  'function' == typeof define && define.amd ? define(['jquery'], e) : 'object' == typeof exports ? module.exports = e(require('jquery')) : e(jQuery);
}(function (e) {
  e.fn.jScrollPane = function (t) {
    function o(t, o) {
      var s,
          n,
          r,
          i,
          a,
          l,
          c,
          p,
          u,
          d,
          f,
          h,
          j,
          g,
          v,
          m,
          w,
          y,
          k,
          b,
          C,
          T,
          S,
          B,
          D,
          x,
          H,
          P,
          A,
          W,
          Y,
          z,
          M,
          X,
          R = this,
          I = !0,
          F = !0,
          L = !1,
          V = !1,
          q = t.clone(!1, !1).empty(),
          O = e.fn.mwheelIntent ? 'mwheelIntent.jsp' : 'mousewheel.jsp';function E(o) {
        var b,
            I,
            F,
            L,
            V,
            q,
            ae,
            le,
            ce,
            pe,
            ue,
            de,
            fe,
            he,
            je,
            ge,
            ve = !1,
            me = !1;if (s = o, void 0 === n) V = t.scrollTop(), q = t.scrollLeft(), t.css({ overflow: 'hidden', padding: 0 }), r = t.innerWidth() + M, i = t.innerHeight(), t.width(r), n = e('<div class="jspPane" />').css('padding', z).append(t.children()), a = e('<div class="jspContainer" />').css({ width: r + 'px', height: i + 'px' }).append(n).appendTo(t);else {
          if (t.css('width', ''), a.css({ width: 'auto', height: 'auto' }), n.css('position', 'static'), ae = t.innerWidth() + M, le = t.innerHeight(), n.css('position', 'absolute'), ve = s.stickToBottom && (pe = c - i) > 20 && pe - re() < 10, me = s.stickToRight && (ce = l - r) > 20 && ce - ne() < 10, L = ae !== r || le !== i, r = ae, i = le, a.css({ width: r, height: i }), !L && X == l && n.outerHeight() == c) return void t.width(r);X = l, n.css('width', ''), t.width(r), a.find('>.jspVerticalBar,>.jspHorizontalBar').remove().end();
        }n.css('overflow', 'auto'), l = o.contentWidth ? o.contentWidth : n[0].scrollWidth, c = n[0].scrollHeight, n.css('overflow', ''), p = l / r, d = (u = c / i) > 1 || s.alwaysShowVScroll, (f = p > 1 || s.alwaysShowHScroll) || d ? (t.addClass('jspScrollable'), (b = s.maintainPosition && (g || w)) && (I = ne(), F = re()), d && (a.append(e('<div class="jspVerticalBar" />').append(e('<div class="jspCap jspCapTop" />'), e('<div class="jspTrack" />').append(e('<div class="jspDrag" />').append(e('<div class="jspDragTop" />'), e('<div class="jspDragBottom" />'))), e('<div class="jspCap jspCapBottom" />'))), y = a.find('>.jspVerticalBar'), k = y.find('>.jspTrack'), h = k.find('>.jspDrag'), s.showArrows && (S = e('<a class="jspArrow jspArrowUp" />').on('mousedown.jsp', Q(0, -1)).on('click.jsp', ie), B = e('<a class="jspArrow jspArrowDown" />').on('mousedown.jsp', Q(0, 1)).on('click.jsp', ie), s.arrowScrollOnHover && (S.on('mouseover.jsp', Q(0, -1, S)), B.on('mouseover.jsp', Q(0, 1, B))), K(k, s.verticalArrowPositions, S, B)), C = i, a.find('>.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow').each(function () {
          C -= e(this).outerHeight();
        }), h.on('mouseenter', function () {
          h.addClass('jspHover');
        }).on('mouseleave', function () {
          h.removeClass('jspHover');
        }).on('mousedown.jsp', function (t) {
          e('html').on('dragstart.jsp selectstart.jsp', ie), h.addClass('jspActive');var o = t.pageY - h.position().top;return e('html').on('mousemove.jsp', function (e) {
            J(e.pageY - o, !1);
          }).on('mouseup.jsp mouseleave.jsp', $), !1;
        }), G()), f && (a.append(e('<div class="jspHorizontalBar" />').append(e('<div class="jspCap jspCapLeft" />'), e('<div class="jspTrack" />').append(e('<div class="jspDrag" />').append(e('<div class="jspDragLeft" />'), e('<div class="jspDragRight" />'))), e('<div class="jspCap jspCapRight" />'))), D = a.find('>.jspHorizontalBar'), x = D.find('>.jspTrack'), v = x.find('>.jspDrag'), s.showArrows && (A = e('<a class="jspArrow jspArrowLeft" />').on('mousedown.jsp', Q(-1, 0)).on('click.jsp', ie), W = e('<a class="jspArrow jspArrowRight" />').on('mousedown.jsp', Q(1, 0)).on('click.jsp', ie), s.arrowScrollOnHover && (A.on('mouseover.jsp', Q(-1, 0, A)), W.on('mouseover.jsp', Q(1, 0, W))), K(x, s.horizontalArrowPositions, A, W)), v.on('mouseenter', function () {
          v.addClass('jspHover');
        }).on('mouseleave', function () {
          v.removeClass('jspHover');
        }).on('mousedown.jsp', function (t) {
          e('html').on('dragstart.jsp selectstart.jsp', ie), v.addClass('jspActive');var o = t.pageX - v.position().left;return e('html').on('mousemove.jsp', function (e) {
            _(e.pageX - o, !1);
          }).on('mouseup.jsp mouseleave.jsp', $), !1;
        }), H = a.innerWidth(), N()), function () {
          if (f && d) {
            var t = x.outerHeight(),
                o = k.outerWidth();C -= t, e(D).find('>.jspCap:visible,>.jspArrow').each(function () {
              H += e(this).outerWidth();
            }), H -= o, i -= o, r -= t, x.parent().append(e('<div class="jspCorner" />').css('width', t + 'px')), G(), N();
          }f && n.width(a.outerWidth() - M + 'px');c = n.outerHeight(), u = c / i, f && ((P = Math.ceil(1 / p * H)) > s.horizontalDragMaxWidth ? P = s.horizontalDragMaxWidth : P < s.horizontalDragMinWidth && (P = s.horizontalDragMinWidth), v.css('width', P + 'px'), m = H - P, ee(w));d && ((T = Math.ceil(1 / u * C)) > s.verticalDragMaxHeight ? T = s.verticalDragMaxHeight : T < s.verticalDragMinHeight && (T = s.verticalDragMinHeight), h.css('height', T + 'px'), j = C - T, Z(g));
        }(), b && (oe(me ? l - r : I, !1), te(ve ? c - i : F, !1)), n.find(':input,a').off('focus.jsp').on('focus.jsp', function (e) {
          se(e.target, !1);
        }), a.off(O).on(O, function (e, t, o, n) {
          w || (w = 0), g || (g = 0);var r = w,
              i = g,
              a = e.deltaFactor || s.mouseWheelSpeed;return R.scrollBy(o * a, -n * a, !1), r == w && i == g;
        }), ge = !1, a.off('touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick').on('touchstart.jsp', function (e) {
          var t = e.originalEvent.touches[0];ue = ne(), de = re(), fe = t.pageX, he = t.pageY, je = !1, ge = !0;
        }).on('touchmove.jsp', function (e) {
          if (ge) {
            var t = e.originalEvent.touches[0],
                o = w,
                s = g;return R.scrollTo(ue + fe - t.pageX, de + he - t.pageY), je = je || Math.abs(fe - t.pageX) > 5 || Math.abs(he - t.pageY) > 5, o == w && s == g;
          }
        }).on('touchend.jsp', function (e) {
          ge = !1;
        }).on('click.jsp-touchclick', function (e) {
          if (je) return je = !1, !1;
        }), s.enableKeyboardNavigation && function () {
          var o,
              r,
              l = [];f && l.push(D[0]);d && l.push(y[0]);n.on('focus.jsp', function () {
            t.focus();
          }), t.attr('tabindex', 0).off('keydown.jsp keypress.jsp').on('keydown.jsp', function (t) {
            if (t.target === this || l.length && e(t.target).closest(l).length) {
              var s = w,
                  n = g;switch (t.keyCode) {case 40:case 38:case 34:case 32:case 33:case 39:case 37:
                  o = t.keyCode, p();break;case 35:
                  te(c - i), o = null;break;case 36:
                  te(0), o = null;}return !(r = t.keyCode == o && s != w || n != g);
            }
          }).on('keypress.jsp', function (t) {
            if (t.keyCode == o && p(), t.target === this || l.length && e(t.target).closest(l).length) return !r;
          }), s.hideFocus ? (t.css('outline', 'none'), 'hideFocus' in a[0] && t.attr('hideFocus', !0)) : (t.css('outline', ''), 'hideFocus' in a[0] && t.attr('hideFocus', !1));function p() {
            var e = w,
                t = g;switch (o) {case 40:
                R.scrollByY(s.keyboardSpeed, !1);break;case 38:
                R.scrollByY(-s.keyboardSpeed, !1);break;case 34:case 32:
                R.scrollByY(i * s.scrollPagePercent, !1);break;case 33:
                R.scrollByY(-i * s.scrollPagePercent, !1);break;case 39:
                R.scrollByX(s.keyboardSpeed, !1);break;case 37:
                R.scrollByX(-s.keyboardSpeed, !1);}return r = e != w || t != g;
          }
        }(), s.clickOnTrack && function () {
          U(), d && k.on('mousedown.jsp', function (t) {
            if (void 0 === t.originalTarget || t.originalTarget == t.currentTarget) {
              var o,
                  n = e(this),
                  r = n.offset(),
                  a = t.pageY - r.top - g,
                  l = !0,
                  p = function () {
                var e = n.offset(),
                    r = t.pageY - e.top - T / 2,
                    d = i * s.scrollPagePercent,
                    f = j * d / (c - i);if (a < 0) g - f > r ? R.scrollByY(-d) : J(r);else {
                  if (!(a > 0)) return void u();g + f < r ? R.scrollByY(d) : J(r);
                }o = setTimeout(p, l ? s.initialDelay : s.trackClickRepeatFreq), l = !1;
              },
                  u = function () {
                o && clearTimeout(o), o = null, e(document).off('mouseup.jsp', u);
              };return p(), e(document).on('mouseup.jsp', u), !1;
            }
          });f && x.on('mousedown.jsp', function (t) {
            if (void 0 === t.originalTarget || t.originalTarget == t.currentTarget) {
              var o,
                  n = e(this),
                  i = n.offset(),
                  a = t.pageX - i.left - w,
                  c = !0,
                  p = function () {
                var e = n.offset(),
                    i = t.pageX - e.left - P / 2,
                    d = r * s.scrollPagePercent,
                    f = m * d / (l - r);if (a < 0) w - f > i ? R.scrollByX(-d) : _(i);else {
                  if (!(a > 0)) return void u();w + f < i ? R.scrollByX(d) : _(i);
                }o = setTimeout(p, c ? s.initialDelay : s.trackClickRepeatFreq), c = !1;
              },
                  u = function () {
                o && clearTimeout(o), o = null, e(document).off('mouseup.jsp', u);
              };return p(), e(document).on('mouseup.jsp', u), !1;
            }
          });
        }(), function () {
          if (location.hash && location.hash.length > 1) {
            var t,
                o,
                s = escape(location.hash.substr(1));try {
              t = e('#' + s + ', a[name="' + s + '"]');
            } catch (e) {
              return;
            }t.length && n.find(s) && (0 === a.scrollTop() ? o = setInterval(function () {
              a.scrollTop() > 0 && (se(t, !0), e(document).scrollTop(a.position().top), clearInterval(o));
            }, 50) : (se(t, !0), e(document).scrollTop(a.position().top)));
          }
        }(), s.hijackInternalLinks && function () {
          if (e(document.body).data('jspHijack')) return;e(document.body).data('jspHijack', !0), e(document.body).delegate('a[href*="#"]', 'click', function (t) {
            var o,
                s,
                n,
                r,
                i,
                a = this.href.substr(0, this.href.indexOf('#')),
                l = location.href;if (-1 !== location.href.indexOf('#') && (l = location.href.substr(0, location.href.indexOf('#'))), a === l) {
              o = escape(this.href.substr(this.href.indexOf('#') + 1));try {
                s = e('#' + o + ', a[name="' + o + '"]');
              } catch (e) {
                return;
              }s.length && (n = s.closest('.jspScrollable'), n.data('jsp').scrollToElement(s, !0), n[0].scrollIntoView && (r = e(window).scrollTop(), ((i = s.offset().top) < r || i > r + e(window).height()) && n[0].scrollIntoView()), t.preventDefault());
            }
          });
        }()) : (t.removeClass('jspScrollable'), n.css({ top: 0, left: 0, width: a.width() - M }), a.off(O), n.find(':input,a').off('focus.jsp'), t.attr('tabindex', '-1').removeAttr('tabindex').off('keydown.jsp keypress.jsp'), n.off('.jsp'), U()), s.autoReinitialise && !Y ? Y = setInterval(function () {
          E(s);
        }, s.autoReinitialiseDelay) : !s.autoReinitialise && Y && clearInterval(Y), V && t.scrollTop(0) && te(V, !1), q && t.scrollLeft(0) && oe(q, !1), t.trigger('jsp-initialised', [f || d]);
      }function G() {
        k.height(C + 'px'), g = 0, b = s.verticalGutter + k.outerWidth(), n.width(r - b - M);try {
          0 === y.position().left && n.css('margin-left', b + 'px');
        } catch (e) {}
      }function N() {
        a.find('>.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow').each(function () {
          H -= e(this).outerWidth();
        }), x.width(H + 'px'), w = 0;
      }function K(e, t, o, s) {
        var n,
            r = 'before',
            i = 'after';'os' == t && (t = /Mac/.test(navigator.platform) ? 'after' : 'split'), t == r ? i = t : t == i && (r = t, n = o, o = s, s = n), e[r](o)[i](s);
      }function Q(t, o, n) {
        return function () {
          return function (t, o, n, r) {
            n = e(n).addClass('jspActive');var i,
                a,
                l = !0,
                c = function () {
              0 !== t && R.scrollByX(t * s.arrowButtonSpeed), 0 !== o && R.scrollByY(o * s.arrowButtonSpeed), a = setTimeout(c, l ? s.initialDelay : s.arrowRepeatFreq), l = !1;
            };c(), i = r ? 'mouseout.jsp' : 'mouseup.jsp', (r = r || e('html')).on(i, function () {
              n.removeClass('jspActive'), a && clearTimeout(a), a = null, r.off(i);
            });
          }(t, o, this, n), this.blur(), !1;
        };
      }function U() {
        x && x.off('mousedown.jsp'), k && k.off('mousedown.jsp');
      }function $() {
        e('html').off('dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp'), h && h.removeClass('jspActive'), v && v.removeClass('jspActive');
      }function J(o, n) {
        if (d) {
          o < 0 ? o = 0 : o > j && (o = j);var r = new e.Event('jsp-will-scroll-y');if (t.trigger(r, [o]), !r.isDefaultPrevented()) {
            var a = o || 0,
                l = 0 === a,
                p = a == j,
                u = -(o / j) * (c - i);void 0 === n && (n = s.animateScroll), n ? R.animate(h, 'top', o, Z, function () {
              t.trigger('jsp-user-scroll-y', [-u, l, p]);
            }) : (h.css('top', o), Z(o), t.trigger('jsp-user-scroll-y', [-u, l, p]));
          }
        }
      }function Z(e) {
        void 0 === e && (e = h.position().top), a.scrollTop(0);var o,
            r,
            l = 0 === (g = e || 0),
            p = g == j,
            u = -(e / j) * (c - i);I == l && L == p || (I = l, L = p, t.trigger('jsp-arrow-change', [I, L, F, V])), o = l, r = p, s.showArrows && (S[o ? 'addClass' : 'removeClass']('jspDisabled'), B[r ? 'addClass' : 'removeClass']('jspDisabled')), n.css('top', u), t.trigger('jsp-scroll-y', [-u, l, p]).trigger('scroll');
      }function _(o, n) {
        if (f) {
          o < 0 ? o = 0 : o > m && (o = m);var i = new e.Event('jsp-will-scroll-x');if (t.trigger(i, [o]), !i.isDefaultPrevented()) {
            var a = o || 0,
                c = 0 === a,
                p = a == m,
                u = -(o / m) * (l - r);void 0 === n && (n = s.animateScroll), n ? R.animate(v, 'left', o, ee, function () {
              t.trigger('jsp-user-scroll-x', [-u, c, p]);
            }) : (v.css('left', o), ee(o), t.trigger('jsp-user-scroll-x', [-u, c, p]));
          }
        }
      }function ee(e) {
        void 0 === e && (e = v.position().left), a.scrollTop(0);var o,
            i,
            c = 0 === (w = e || 0),
            p = w == m,
            u = -(e / m) * (l - r);F == c && V == p || (F = c, V = p, t.trigger('jsp-arrow-change', [I, L, F, V])), o = c, i = p, s.showArrows && (A[o ? 'addClass' : 'removeClass']('jspDisabled'), W[i ? 'addClass' : 'removeClass']('jspDisabled')), n.css('left', u), t.trigger('jsp-scroll-x', [-u, c, p]).trigger('scroll');
      }function te(e, t) {
        J(e / (c - i) * j, t);
      }function oe(e, t) {
        _(e / (l - r) * m, t);
      }function se(t, o, n) {
        var l,
            c,
            p,
            u,
            d,
            f,
            h,
            j,
            g,
            v = 0,
            m = 0;try {
          l = e(t);
        } catch (e) {
          return;
        }for (c = l.outerHeight(), p = l.outerWidth(), a.scrollTop(0), a.scrollLeft(0); !l.is('.jspPane');) if (v += l.position().top, m += l.position().left, l = l.offsetParent(), /^body|html$/i.test(l[0].nodeName)) return;f = (u = re()) + i, v < u || o ? j = v - s.horizontalGutter : v + c > f && (j = v - i + c + s.horizontalGutter), isNaN(j) || te(j, n), h = (d = ne()) + r, m < d || o ? g = m - s.horizontalGutter : m + p > h && (g = m - r + p + s.horizontalGutter), isNaN(g) || oe(g, n);
      }function ne() {
        return -n.position().left;
      }function re() {
        return -n.position().top;
      }function ie() {
        return !1;
      }'border-box' === t.css('box-sizing') ? (z = 0, M = 0) : (z = t.css('paddingTop') + ' ' + t.css('paddingRight') + ' ' + t.css('paddingBottom') + ' ' + t.css('paddingLeft'), M = (parseInt(t.css('paddingLeft'), 10) || 0) + (parseInt(t.css('paddingRight'), 10) || 0)), e.extend(R, { reinitialise: function (t) {
          E(t = e.extend({}, s, t));
        }, scrollToElement: function (e, t, o) {
          se(e, t, o);
        }, scrollTo: function (e, t, o) {
          oe(e, o), te(t, o);
        }, scrollToX: function (e, t) {
          oe(e, t);
        }, scrollToY: function (e, t) {
          te(e, t);
        }, scrollToPercentX: function (e, t) {
          oe(e * (l - r), t);
        }, scrollToPercentY: function (e, t) {
          te(e * (c - i), t);
        }, scrollBy: function (e, t, o) {
          R.scrollByX(e, o), R.scrollByY(t, o);
        }, scrollByX: function (e, t) {
          _((ne() + Math[e < 0 ? 'floor' : 'ceil'](e)) / (l - r) * m, t);
        }, scrollByY: function (e, t) {
          J((re() + Math[e < 0 ? 'floor' : 'ceil'](e)) / (c - i) * j, t);
        }, positionDragX: function (e, t) {
          _(e, t);
        }, positionDragY: function (e, t) {
          J(e, t);
        }, animate: function (e, t, o, n, r) {
          var i = {};i[t] = o, e.animate(i, { duration: s.animateDuration, easing: s.animateEase, queue: !1, step: n, complete: r });
        }, getContentPositionX: function () {
          return ne();
        }, getContentPositionY: function () {
          return re();
        }, getContentWidth: function () {
          return l;
        }, getContentHeight: function () {
          return c;
        }, getPercentScrolledX: function () {
          return ne() / (l - r);
        }, getPercentScrolledY: function () {
          return re() / (c - i);
        }, getIsScrollableH: function () {
          return f;
        }, getIsScrollableV: function () {
          return d;
        }, getContentPane: function () {
          return n;
        }, scrollToBottom: function (e) {
          J(j, e);
        }, hijackInternalLinks: e.noop, destroy: function () {
          var e, o;e = re(), o = ne(), t.removeClass('jspScrollable').off('.jsp'), n.off('.jsp'), t.replaceWith(q.append(n.children())), q.scrollTop(e), q.scrollLeft(o), Y && clearInterval(Y);
        } }), E(o);
    }return t = e.extend({}, e.fn.jScrollPane.defaults, t), e.each(['arrowButtonSpeed', 'trackClickSpeed', 'keyboardSpeed'], function () {
      t[this] = t[this] || t.speed;
    }), this.each(function () {
      var s = e(this),
          n = s.data('jsp');n ? n.reinitialise(t) : (e('script', s).filter('[type="text/javascript"],:not([type])').remove(), n = new o(s, t), s.data('jsp', n));
    });
  }, e.fn.jScrollPane.defaults = { showArrows: !1, maintainPosition: !0, stickToBottom: !1, stickToRight: !1, clickOnTrack: !0, autoReinitialise: !1, autoReinitialiseDelay: 500, verticalDragMinHeight: 0, verticalDragMaxHeight: 99999, horizontalDragMinWidth: 0, horizontalDragMaxWidth: 99999, contentWidth: void 0, animateScroll: !1, animateDuration: 300, animateEase: 'linear', hijackInternalLinks: !1, verticalGutter: 4, horizontalGutter: 4, mouseWheelSpeed: 3, arrowButtonSpeed: 0, arrowRepeatFreq: 50, arrowScrollOnHover: !1, trackClickSpeed: 0, trackClickRepeatFreq: 70, verticalArrowPositions: 'split', horizontalArrowPositions: 'split', enableKeyboardNavigation: !0, hideFocus: !1, keyboardSpeed: 0, initialDelay: 300, speed: 30, scrollPagePercent: .8, alwaysShowVScroll: !1, alwaysShowHScroll: !1 };
});
//# sourceMappingURL=jquery.jscrollpane.min.js.map

(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['module', 'exports'], factory);
    } else if (typeof exports !== "undefined") {
        factory(module, exports);
    } else {
        var mod = {
            exports: {}
        };
        factory(mod, mod.exports);
        global.WOW = mod.exports;
    }
})(this, function (module, exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _class, _temp;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function isIn(needle, haystack) {
        return haystack.indexOf(needle) >= 0;
    }

    function extend(custom, defaults) {
        for (var key in defaults) {
            if (custom[key] == null) {
                var value = defaults[key];
                custom[key] = value;
            }
        }
        return custom;
    }

    function isMobile(agent) {
        return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(agent)
        );
    }

    function createEvent(event) {
        var bubble = arguments.length <= 1 || arguments[1] === undefined ? false : arguments[1];
        var cancel = arguments.length <= 2 || arguments[2] === undefined ? false : arguments[2];
        var detail = arguments.length <= 3 || arguments[3] === undefined ? null : arguments[3];

        var customEvent = void 0;
        if (document.createEvent != null) {
            // W3C DOM
            customEvent = document.createEvent('CustomEvent');
            customEvent.initCustomEvent(event, bubble, cancel, detail);
        } else if (document.createEventObject != null) {
            // IE DOM < 9
            customEvent = document.createEventObject();
            customEvent.eventType = event;
        } else {
            customEvent.eventName = event;
        }

        return customEvent;
    }

    function emitEvent(elem, event) {
        if (elem.dispatchEvent != null) {
            // W3C DOM
            elem.dispatchEvent(event);
        } else if (event in (elem != null)) {
            elem[event]();
        } else if ('on' + event in (elem != null)) {
            elem['on' + event]();
        }
    }

    function addEvent(elem, event, fn) {
        if (elem.addEventListener != null) {
            // W3C DOM
            elem.addEventListener(event, fn, false);
        } else if (elem.attachEvent != null) {
            // IE DOM
            elem.attachEvent('on' + event, fn);
        } else {
            // fallback
            elem[event] = fn;
        }
    }

    function removeEvent(elem, event, fn) {
        if (elem.removeEventListener != null) {
            // W3C DOM
            elem.removeEventListener(event, fn, false);
        } else if (elem.detachEvent != null) {
            // IE DOM
            elem.detachEvent('on' + event, fn);
        } else {
            // fallback
            delete elem[event];
        }
    }

    function getInnerHeight() {
        if ('innerHeight' in window) {
            return window.innerHeight;
        }

        return document.documentElement.clientHeight;
    }

    // Minimalistic WeakMap shim, just in case.
    var WeakMap = window.WeakMap || window.MozWeakMap || function () {
        function WeakMap() {
            _classCallCheck(this, WeakMap);

            this.keys = [];
            this.values = [];
        }

        _createClass(WeakMap, [{
            key: 'get',
            value: function get(key) {
                for (var i = 0; i < this.keys.length; i++) {
                    var item = this.keys[i];
                    if (item === key) {
                        return this.values[i];
                    }
                }
                return undefined;
            }
        }, {
            key: 'set',
            value: function set(key, value) {
                for (var i = 0; i < this.keys.length; i++) {
                    var item = this.keys[i];
                    if (item === key) {
                        this.values[i] = value;
                        return this;
                    }
                }
                this.keys.push(key);
                this.values.push(value);
                return this;
            }
        }]);

        return WeakMap;
    }();

    // Dummy MutationObserver, to avoid raising exceptions.
    var MutationObserver = window.MutationObserver || window.WebkitMutationObserver || window.MozMutationObserver || (_temp = _class = function () {
        function MutationObserver() {
            _classCallCheck(this, MutationObserver);

            if (typeof console !== 'undefined' && console !== null) {
                console.warn('MutationObserver is not supported by your browser.');
                console.warn('WOW.js cannot detect dom mutations, please call .sync() after loading new content.');
            }
        }

        _createClass(MutationObserver, [{
            key: 'observe',
            value: function observe() {}
        }]);

        return MutationObserver;
    }(), _class.notSupported = true, _temp);

    // getComputedStyle shim, from http://stackoverflow.com/a/21797294
    var getComputedStyle = window.getComputedStyle || function getComputedStyle(el) {
        var getComputedStyleRX = /(\-([a-z]){1})/g;
        return {
            getPropertyValue: function getPropertyValue(prop) {
                if (prop === 'float') {
                    prop = 'styleFloat';
                }
                if (getComputedStyleRX.test(prop)) {
                    prop.replace(getComputedStyleRX, function (_, _char) {
                        return _char.toUpperCase();
                    });
                }
                var currentStyle = el.currentStyle;

                return (currentStyle != null ? currentStyle[prop] : void 0) || null;
            }
        };
    };

    var WOW = function () {
        function WOW() {
            var options = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

            _classCallCheck(this, WOW);

            this.defaults = {
                boxClass: 'wow',
                animateClass: 'animated',
                offset: 0,
                mobile: true,
                live: true,
                callback: null,
                scrollContainer: null,
                resetAnimation: true
            };

            this.animate = function animateFactory() {
                if ('requestAnimationFrame' in window) {
                    return function (callback) {
                        return window.requestAnimationFrame(callback);
                    };
                }
                return function (callback) {
                    return callback();
                };
            }();

            this.vendors = ['moz', 'webkit'];

            this.start = this.start.bind(this);
            this.resetAnimation = this.resetAnimation.bind(this);
            this.scrollHandler = this.scrollHandler.bind(this);
            this.scrollCallback = this.scrollCallback.bind(this);
            this.scrolled = true;
            this.config = extend(options, this.defaults);
            if (options.scrollContainer != null) {
                this.config.scrollContainer = document.querySelector(options.scrollContainer);
            }
            // Map of elements to animation names:
            this.animationNameCache = new WeakMap();
            this.wowEvent = createEvent(this.config.boxClass);
        }

        _createClass(WOW, [{
            key: 'init',
            value: function init() {
                this.element = window.document.documentElement;
                if (isIn(document.readyState, ['interactive', 'complete'])) {
                    this.start();
                } else {
                    addEvent(document, 'DOMContentLoaded', this.start);
                }
                this.finished = [];
            }
        }, {
            key: 'start',
            value: function start() {
                var _this = this;

                this.stopped = false;
                this.boxes = [].slice.call(this.element.querySelectorAll('.' + this.config.boxClass));
                this.all = this.boxes.slice(0);
                if (this.boxes.length) {
                    if (this.disabled()) {
                        this.resetStyle();
                    } else {
                        for (var i = 0; i < this.boxes.length; i++) {
                            var box = this.boxes[i];
                            this.applyStyle(box, true);
                        }
                    }
                }
                if (!this.disabled()) {
                    addEvent(this.config.scrollContainer || window, 'scroll', this.scrollHandler);
                    addEvent(window, 'resize', this.scrollHandler);
                    this.interval = setInterval(this.scrollCallback, 50);
                }
                if (this.config.live) {
                    var mut = new MutationObserver(function (records) {
                        for (var j = 0; j < records.length; j++) {
                            var record = records[j];
                            for (var k = 0; k < record.addedNodes.length; k++) {
                                var node = record.addedNodes[k];
                                _this.doSync(node);
                            }
                        }
                        return undefined;
                    });
                    mut.observe(document.body, {
                        childList: true,
                        subtree: true
                    });
                }
            }
        }, {
            key: 'stop',
            value: function stop() {
                this.stopped = true;
                removeEvent(this.config.scrollContainer || window, 'scroll', this.scrollHandler);
                removeEvent(window, 'resize', this.scrollHandler);
                if (this.interval != null) {
                    clearInterval(this.interval);
                }
            }
        }, {
            key: 'sync',
            value: function sync() {
                if (MutationObserver.notSupported) {
                    this.doSync(this.element);
                }
            }
        }, {
            key: 'doSync',
            value: function doSync(element) {
                if (typeof element === 'undefined' || element === null) {
                    element = this.element;
                }
                if (element.nodeType !== 1) {
                    return;
                }
                element = element.parentNode || element;
                var iterable = element.querySelectorAll('.' + this.config.boxClass);
                for (var i = 0; i < iterable.length; i++) {
                    var box = iterable[i];
                    if (!isIn(box, this.all)) {
                        this.boxes.push(box);
                        this.all.push(box);
                        if (this.stopped || this.disabled()) {
                            this.resetStyle();
                        } else {
                            this.applyStyle(box, true);
                        }
                        this.scrolled = true;
                    }
                }
            }
        }, {
            key: 'show',
            value: function show(box) {
                this.applyStyle(box);
                box.className = box.className + ' ' + this.config.animateClass;
                if (this.config.callback != null) {
                    this.config.callback(box);
                }
                emitEvent(box, this.wowEvent);

                if (this.config.resetAnimation) {
                    addEvent(box, 'animationend', this.resetAnimation);
                    addEvent(box, 'oanimationend', this.resetAnimation);
                    addEvent(box, 'webkitAnimationEnd', this.resetAnimation);
                    addEvent(box, 'MSAnimationEnd', this.resetAnimation);
                }

                return box;
            }
        }, {
            key: 'applyStyle',
            value: function applyStyle(box, hidden) {
                var _this2 = this;

                var duration = box.getAttribute('data-wow-duration');
                var delay = box.getAttribute('data-wow-delay');
                var iteration = box.getAttribute('data-wow-iteration');

                return this.animate(function () {
                    return _this2.customStyle(box, hidden, duration, delay, iteration);
                });
            }
        }, {
            key: 'resetStyle',
            value: function resetStyle() {
                for (var i = 0; i < this.boxes.length; i++) {
                    var box = this.boxes[i];
                    box.style.visibility = 'visible';
                }
                return undefined;
            }
        }, {
            key: 'resetAnimation',
            value: function resetAnimation(event) {
                if (event.type.toLowerCase().indexOf('animationend') >= 0) {
                    var target = event.target || event.srcElement;
                    target.className = target.className.replace(this.config.animateClass, '').trim();
                }
            }
        }, {
            key: 'customStyle',
            value: function customStyle(box, hidden, duration, delay, iteration) {
                if (hidden) {
                    this.cacheAnimationName(box);
                }
                box.style.visibility = hidden ? 'hidden' : 'visible';

                if (duration) {
                    this.vendorSet(box.style, { animationDuration: duration });
                }
                if (delay) {
                    this.vendorSet(box.style, { animationDelay: delay });
                }
                if (iteration) {
                    this.vendorSet(box.style, { animationIterationCount: iteration });
                }
                this.vendorSet(box.style, { animationName: hidden ? 'none' : this.cachedAnimationName(box) });

                return box;
            }
        }, {
            key: 'vendorSet',
            value: function vendorSet(elem, properties) {
                for (var name in properties) {
                    if (properties.hasOwnProperty(name)) {
                        var value = properties[name];
                        elem['' + name] = value;
                        for (var i = 0; i < this.vendors.length; i++) {
                            var vendor = this.vendors[i];
                            elem['' + vendor + name.charAt(0).toUpperCase() + name.substr(1)] = value;
                        }
                    }
                }
            }
        }, {
            key: 'vendorCSS',
            value: function vendorCSS(elem, property) {
                var style = getComputedStyle(elem);
                var result = style.getPropertyCSSValue(property);
                for (var i = 0; i < this.vendors.length; i++) {
                    var vendor = this.vendors[i];
                    result = result || style.getPropertyCSSValue('-' + vendor + '-' + property);
                }
                return result;
            }
        }, {
            key: 'animationName',
            value: function animationName(box) {
                var aName = void 0;
                try {
                    aName = this.vendorCSS(box, 'animation-name').cssText;
                } catch (error) {
                    // Opera, fall back to plain property value
                    aName = getComputedStyle(box).getPropertyValue('animation-name');
                }

                if (aName === 'none') {
                    return ''; // SVG/Firefox, unable to get animation name?
                }

                return aName;
            }
        }, {
            key: 'cacheAnimationName',
            value: function cacheAnimationName(box) {
                // https://bugzilla.mozilla.org/show_bug.cgi?id=921834
                // box.dataset is not supported for SVG elements in Firefox
                return this.animationNameCache.set(box, this.animationName(box));
            }
        }, {
            key: 'cachedAnimationName',
            value: function cachedAnimationName(box) {
                return this.animationNameCache.get(box);
            }
        }, {
            key: 'scrollHandler',
            value: function scrollHandler() {
                this.scrolled = true;
            }
        }, {
            key: 'scrollCallback',
            value: function scrollCallback() {
                if (this.scrolled) {
                    this.scrolled = false;
                    var results = [];
                    for (var i = 0; i < this.boxes.length; i++) {
                        var box = this.boxes[i];
                        if (box) {
                            if (this.isVisible(box)) {
                                this.show(box);
                                continue;
                            }
                            results.push(box);
                        }
                    }
                    this.boxes = results;
                    if (!this.boxes.length && !this.config.live) {
                        this.stop();
                    }
                }
            }
        }, {
            key: 'offsetTop',
            value: function offsetTop(element) {
                // SVG elements don't have an offsetTop in Firefox.
                // This will use their nearest parent that has an offsetTop.
                // Also, using ('offsetTop' of element) causes an exception in Firefox.
                while (element.offsetTop === undefined) {
                    element = element.parentNode;
                }
                var top = element.offsetTop;
                while (element.offsetParent) {
                    element = element.offsetParent;
                    top += element.offsetTop;
                }
                return top;
            }
        }, {
            key: 'isVisible',
            value: function isVisible(box) {
                var offset = box.getAttribute('data-wow-offset') || this.config.offset;
                var viewTop = this.config.scrollContainer && this.config.scrollContainer.scrollTop || window.pageYOffset;
                var viewBottom = viewTop + Math.min(this.element.clientHeight, getInnerHeight()) - offset;
                var top = this.offsetTop(box);
                var bottom = top + box.clientHeight;

                return top <= viewBottom && bottom >= viewTop;
            }
        }, {
            key: 'disabled',
            value: function disabled() {
                return !this.config.mobile && isMobile(navigator.userAgent);
            }
        }]);

        return WOW;
    }();

    exports.default = WOW;
    module.exports = exports['default'];
});
//# sourceMappingURL=wow.js.map

$(document).ready(function () {
    function sliderBlock() {
        // slider product
        $('.slider-block').not('.slick-initialized').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            nextArrow: '<button type="button" class="slick-next"></button>',
            prevArrow: '<button type="button" class="slick-prev"></button>',
            dots: false,
            infinite: true,
            responsive: [{
                breakpoint: 1201,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    arrows: false
                }
            }, {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: false
                }
            }, {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    variableWidth: true
                }
            }]
        });
        console.log('call');
    }

    var resizeId;
    $(window).resize(function () {
        clearTimeout(resizeId);
        resizeId = setTimeout(function () {
            sliderBlock();
        });
    });
    sliderBlock();
});

$(window).on('load', function () {});
//# sourceMappingURL=product.js.map

'use strict';

var isMobileScreen = function () {
    return document.body.clientWidth < 992;
};

var app = {
    init: function () {
        this.slider();
        this.calcVH();
        this.stickyHeader();

        this.selectStyle();
    },
    slider: function () {
        // $('.ul-hot-products-mobile').slick( {
        //     dots: true,
        //     arrows: false,
        //     infinite: true,
        //     slidesToShow: 1,
        //     slidesToScroll: 1,
        //     autoplay: true,
        //     autoplaySpeed: 3000
        // });


    },
    closeMenuMobile: function () {
        $('.mobile-nav').removeClass('show');
        $('html, body').removeClass('open-accordion');
        $('.nav-mobile').removeClass('cancel-nav-mobile');
    },
    menuMobile: function () {
        $('.nav-mobile').click(function () {
            if (!$(this).hasClass('cancel-nav-mobile')) {
                console.log('dang dong');
                $(this).addClass('cancel-nav-mobile');
                $('.mobile-nav').addClass('show');
                $('html, body').addClass('open-accordion');
            } else {
                console.log('dang mo');
                $('.mobile-nav').removeClass('show');
                $('html, body').removeClass('open-accordion');
                $('.nav-mobile').removeClass('cancel-nav-mobile');
            }
        });
        $('.cancel-nav-mobile,.overlay-mobile-nav').click(function () {
            app.closeMenuMobile();
        });
        $('.card.no-sub-nav .card-header a').click(function () {
            app.closeMenuMobile();
        });
        $('.ul-products li').click(function () {
            app.closeMenuMobile();
        });
    },

    calcVH: function () {
        // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
        var vh = window.innerHeight * 0.01;
        // Then we set the value in the --vh custom property to the root of the document
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    },
    preventClickInsideDropdown: function () {
        if ($('.dropdown-menu').length) {
            $('.dropdown-menu.mega-dropdown-menu').on('click', function (event) {
                // The event won't be propagated up to the document NODE and
                // therefore delegated events won't be fired
                console.log('prevent');
                event.stopPropagation();
            });
        }
    },
    stickyHeader: function () {
        $(window).scroll(function () {
            var sticky = $('.sticky'),
                scroll = $(window).scrollTop();
            if (scroll >= 100) sticky.addClass('fixed');else sticky.removeClass('fixed');
        });
    },
    selectStyle: function () {
        $('.js-select-nn').select2({
            minimumResultsForSearch: Infinity
        });
    }
};

$(document).ready(function () {
    app.init();
    app.menuMobile();
    var wow = new WOW({
        boxClass: 'wow', // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 0, // distance to the element when triggering the animation (default is 0)
        mobile: true, // trigger animations on mobile devices (default is true)
        live: true, // act on asynchronously loaded content (default is true)
        callback: function (box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null, // optional scroll container selector, otherwise use window,
        resetAnimation: true // reset animation on end (default is true)
    });
    wow.init();
    app.preventClickInsideDropdown();

    var sticky = $('.sticky'),
        scroll = $(window).scrollTop();
    if (scroll >= 100) sticky.addClass('fixed');else sticky.removeClass('fixed');

    var resizeId;
    $(window).resize(function () {
        clearTimeout(resizeId);
        resizeId = setTimeout(function () {
            app.slider();
            slick_on_mobile(slick_slider, settings_slider);

            app.calcVH();
            app.stickyHeader();
        });
    });

    $(window).scroll(function () {
        // app.stickyHeader();
    });
});

$(window).on('load', function () {});
//# sourceMappingURL=main.js.map
