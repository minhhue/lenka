$(document).ready(function () {
    function sliderBlock(){
        // slider product
        $('.slider-block').not('.slick-initialized').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            nextArrow: '<button type="button" class="slick-next"></button>',
            prevArrow: '<button type="button" class="slick-prev"></button>',
            dots: false,
            infinite: true,
            responsive: [
                {
                    breakpoint: 1201,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        arrows: false
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        variableWidth: true
                    }
                }]
        });
        console.log('call');
    }

    var resizeId;
    $(window).resize(function () {
        clearTimeout(resizeId);
        resizeId = setTimeout(function () {
            sliderBlock();
        });
    });
    sliderBlock();
});

$(window).on('load', function () {

});

