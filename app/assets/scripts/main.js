'use strict';
var isMobileScreen = function () {
    return document.body.clientWidth < 992;
};

var app = {
    init: function () {
        this.slider();
        this.calcVH();
        this.stickyHeader();

        this.selectStyle();
    },
    slider: function() {
        // $('.ul-hot-products-mobile').slick( {
        //     dots: true,
        //     arrows: false,
        //     infinite: true,
        //     slidesToShow: 1,
        //     slidesToScroll: 1,
        //     autoplay: true,
        //     autoplaySpeed: 3000
        // });



    },
    closeMenuMobile: function(){
        $('.mobile-nav').removeClass('show');
        $('html, body').removeClass('open-accordion');
        $('.nav-mobile').removeClass('cancel-nav-mobile');
    },
    menuMobile: function() {
        $('.nav-mobile').click(function(){
            if (!$(this).hasClass('cancel-nav-mobile')){
                console.log('dang dong');
                $(this).addClass('cancel-nav-mobile');
                $('.mobile-nav').addClass('show');
                $('html, body').addClass('open-accordion');
            }else{
                console.log('dang mo');
                $('.mobile-nav').removeClass('show');
                $('html, body').removeClass('open-accordion');
                $('.nav-mobile').removeClass('cancel-nav-mobile');
            }
        });
        $('.cancel-nav-mobile,.overlay-mobile-nav').click(function(){
            app.closeMenuMobile();
        });
        $('.card.no-sub-nav .card-header a').click(function(){
            app.closeMenuMobile();
        });
        $('.ul-products li').click(function(){
            app.closeMenuMobile();
        });
    },

    calcVH: function () {
        // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
        var vh = window.innerHeight * 0.01;
        // Then we set the value in the --vh custom property to the root of the document
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    },
    preventClickInsideDropdown: function(){
        if ($('.dropdown-menu').length ) {
            $('.dropdown-menu.mega-dropdown-menu').on('click', function(event){
                // The event won't be propagated up to the document NODE and
                // therefore delegated events won't be fired
                console.log('prevent');
                event.stopPropagation();
            });
        }
    },
    stickyHeader: function(){
        $(window).scroll(function () {
            var sticky = $('.sticky'),
                scroll = $(window).scrollTop();
            if (scroll >= 100) sticky.addClass('fixed');
            else sticky.removeClass('fixed');
        });
    },
    selectStyle: function(){
        $('.js-select-nn').select2({
            minimumResultsForSearch: Infinity
        });
    }
};

$(document).ready(function () {
    app.init();
    app.menuMobile();
    var wow = new WOW(
        {
            boxClass: 'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 0,          // distance to the element when triggering the animation (default is 0)
            mobile: true,       // trigger animations on mobile devices (default is true)
            live: true,       // act on asynchronously loaded content (default is true)
            callback: function (box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null,    // optional scroll container selector, otherwise use window,
            resetAnimation: true,     // reset animation on end (default is true)
        }
    );
    wow.init();
    app.preventClickInsideDropdown();

    var sticky = $('.sticky'),
        scroll = $(window).scrollTop();
    if (scroll >= 100) sticky.addClass('fixed');
    else sticky.removeClass('fixed');

    var resizeId;
    $(window).resize(function () {
        clearTimeout(resizeId);
        resizeId = setTimeout(function () {
            app.slider();
            slick_on_mobile( slick_slider, settings_slider);

            app.calcVH();
            app.stickyHeader();
        });
    });

    $(window).scroll(function () {
        // app.stickyHeader();
    });



});

$(window).on('load', function () {

});

